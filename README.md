# Comandos NPM
## _Comando utilizados en este proyecto_

# Packacke.json
Crear package.json eligiendo por default todas las opciones
```sh
$ npm init -y
```

# Instalr dependencias
Para instalar una dependencia se puede hacer de dos formas:
```sh
$ npm i package-name
$ npm install package-name
```

Para instalar una versión especifica de una dependencia se puede hacer de dos formas:
```sh
$ npm i package-name@x.x.x
$ npm install package-name@x.x.x
```

Para indicar que instalarás un dependencia de proyecto sería. "Son necesarias para que el proyecto funcione". Estarán listadas en *dependencies*:
```sh
$ npm i package-name --save
```

Para indicar que instalarás un dependencia de desarrollo en tu proyecto sería. Sólo las ocupa el desarrollador en tiempo de construcción del proyecto. Estarán listadas en *devDependencies*:
```sh
$ npm i package-name --save-dev
```

Para eliminar / desinstalar una dependencia se hará así:
```sh
$ npm uninstall package-name
```

# Ejecutar tarea
Para ejecutar una tarea / script de mi proyecto (package.json > scripts) se hará con:
```sh
$ npm run script-name
```

# Webpack modo desarrollo y productivo

````css
"scripts": {
    "start:dev": "webpack serve --config config/webpack.config.dev.js --mode=development"
}
````

````css
"scripts": {
    "stats": "webpack --config config/webpack.config.prod.js --mode=production --json > dist/stats.json && webpack-bundle-analyzer dist/stats.json"
}
````