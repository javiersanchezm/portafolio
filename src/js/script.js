const hamburguer = document.querySelector('.fa-bars');
const menu = document.querySelector('.list');

hamburguer.addEventListener('click', ()=> {
    menu.classList.add("mostrar")
})

window.addEventListener('click', e =>{
    if(menu.classList.contains('mostrar') && e.target != menu && e.target != hamburguer)
    {
        menu.classList.toggle("mostrar")
    }
})

const html5 = document.getElementById('html5');
const css3 = document.getElementById('css3');
const sass = document.getElementById('sass');
const javascript = document.getElementById('javascript');
const typescript = document.getElementById('typescript');
const webpack = document.getElementById('webpack');
const vite = document.getElementById('vite');
const angular = document.getElementById('angular');
const lit = document.getElementById('lit');

const portafolio = document.getElementById('portafolio');
const article = document.querySelectorAll('article');

portafolio.addEventListener('click', (event) => {
    if(event.target === html5){
        article.forEach( (val, index) => {
            article[index].classList.remove('hidden');
        });
    }

    else if(event.target === css3){
        article.forEach( (val, index) => {
            if( index === 1 || index === 3 || index === 6 || index === 7){
                article[index].classList.remove('hidden');
            } else{
                article[index].classList.add('hidden');
            }
        });
    }

    else if(event.target === sass){
        article.forEach( (val, index) => {
            if( index === 0 || index === 2 || index === 4 || index == 5 ){
                article[index].classList.remove('hidden');
            } else{
                article[index].classList.add('hidden');
            }
        });
    }

    else if(event.target === javascript){
        article.forEach( (val, index) => {
            if( index === 1 || index === 2 || index === 3 || index === 6 || index === 7 ){
                article[index].classList.remove('hidden');
            } else{
                article[index].classList.add('hidden');
            }
        });
    }

    else if(event.target === typescript){
        article.forEach( (val, index) => {
            if( index === 0 || index === 4 || index === 5 ){
                article[index].classList.remove('hidden');
            } else{
                article[index].classList.add('hidden');
            }
        });
    }

    else if(event.target === webpack){
        article.forEach( (val, index) => {
            if( index === 0 || index === 2 || index === 4 || index === 5 ){
                article[index].classList.remove('hidden');
            } else{
                article[index].classList.add('hidden');
            }
        });
    }

    else if(event.target === vite){
        article.forEach( (val, index) => {
            if( index === 1 || index === 2 ){
                console.log(index)
                article[index].classList.remove('hidden');
            } else{
                article[index].classList.add('hidden');
            }
        });
    }

    else if(event.target === angular){
        article.forEach( (val, index) => {
            if( index === 0 || index === 4 || index === 5 ){
                console.log(index)
                article[index].classList.remove('hidden');
            } else{
                article[index].classList.add('hidden');
            }
        });
    }

    else if(event.target === lit){
        article.forEach( (val, index) => {
            if( index === 1 ){
                console.log(index)
                article[index].classList.remove('hidden');
            } else{
                article[index].classList.add('hidden');
            }
        });
    };

});